Este documento apresenta os passos para execução do algoritmo sequencial e paralelo, disponíveis em https://gitlab.com/matheusgsb/Trab02-Grupo25-B.git. 
<br /><br />
Para rodar o algoritmo sequencial, seguir os passos:

1) Abra o terminal no local que contém o arquivo sequencial.c 
<br />2) Para compilar:	gcc sequencial.c -o sequencial
<br />3) Para executar:
<br />    a) ./sequencial                 	    (lê da imagem in.ppm)
<br />    b) ./sequencial entrada.ppm     		(lê da imagem entrada.ppm fornecida)
<br />    c) ./sequencial entrada.ppm saida.ppm   (lê da imagem entrada.ppm e salva na imagem saida.ppm)
<br /><br />
Para rodar o algoritmo paralelo, consideramos que você já tenha instalado em sua máquina openMPI (disponível em: http://www.open-mpi.org/software/ompi/v1.10/).
<br /><br />Caso tenha alguma dúvida na instalação, seguir os passos contidos no link a seguir:  http://www.itp.phys.ethz.ch/education/hs12/programming_techniques/openmpi.pdf
<br /><br />Em seguida, realizar os passos abaixo:

1) Abra o terminal no local que contém o arquivo paralelo.c
<br />2) Para compilar:	mpicc -fopenmp paralelo.c -o paralelo
<br />3) Para executar:
<br />    a) mpirun -np 4 paralelo    		        (para executar com 4 processos)
<br />    b) mpirun -hostfile hosts10 -np 4 paralelo	(para incluir uma lista de nós)
<br />    c) mpirun -np 4 paralelo entrada.ppm saida.ppm     (para especificar entrada e saída)

<br />
Para as imagens em tons de cinza, favor utilizar imagens PGM P5 e os arquivos sequencial_gs.c e paralelo_gs.c de forma análoga.