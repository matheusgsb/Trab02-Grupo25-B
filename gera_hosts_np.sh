#!/bin/bash
declare -a np=('2' '4' '6' '8' '10' '20' '40' '60' '80');
declare -a threads=('3' '6' '9' '12' '15');
declare -a hosts=('5' '10' '15' '19');

for f in `seq 1 4;`
do
	    for n in ${np[@]}
	    do
    	    echo "mpirun -hostfile hosts10 -np $n ./paralelo/paralelo ./imagens/$f.ppm ./resultados/np/foto$f.ppm 3 > ./resultados/np/foto$f-hosts10-np$n-threads3.txt" >> cmd_np.sh
	    done
done

for f in `seq 1 4;`
do
	    for t in ${threads[@]}
	    do
    	    echo "mpirun -hostfile hosts10 -np 20 ./paralelo/paralelo ./imagens/$f.ppm ./resultados/thread/foto$f.ppm $t > ./resultados/thread/foto$f-hosts10-np20-threads$t.txt" >> cmd_threads.sh
	    done
done

for f in `seq 1 4;`
do
	    for h in ${hosts[@]}
	    do
	        echo "mpirun -hostfile hosts$h -np 20 ./paralelo/paralelo ./imagens/$f.ppm ./resultados/hosts/foto$f.ppm 3 > ./resultados/hosts/foto$f-hosts$h-np20-threads3.txt" >> cmd_hosts.sh
	    done
done
