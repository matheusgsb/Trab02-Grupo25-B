#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef unsigned char pixel;

typedef struct {
    int lin;
    int col;
    int max;
    pixel *pixels;
} imagem_gs;

/* Aloca espaco na memoria para uma imagem com 'lin' linhas e 'col' colunas */
imagem_gs *alocaImagem(int lin, int col, int max) {

    imagem_gs *img = (imagem_gs *)malloc(sizeof(imagem_gs));
    if (!img)
        return NULL;
        
    img->lin = lin;
    img->col = col;
    img->max = max;
    img->pixels = (pixel *)malloc(lin * col * sizeof(pixel));
    if (!img->pixels)
        return NULL;
        
    return img;

}

/* Libera o espaco alocado para a imagem */
void liberaImagem(imagem_gs *img) {
    if (img != NULL) {
        free(img->pixels);
        free(img);
    }
}

/* Simula o acesso de uma matriz, retornando o pixel na posicao (i,j) */
pixel obterPixel(imagem_gs *img, int i, int j) {
    
    return img->pixels[(img->col * i) + j];    
  
}

/* Altera o pixel (i,j) na imagem */
void salvarPixel(imagem_gs *img, int i, int j, pixel P) {

    img->pixels[(img->col * i) + j] = P;
    
}

/* Verifica se a imagem e PPM e esta no formato P6 */
int testaFormatoImagem(FILE *arquivo) {

    char c;
    char comentarios[1024];
    
    c = getc(arquivo);
    if (c=='P' || c=='p') {
        c = getc(arquivo);
        if (c=='5') {
            c = getc(arquivo);
            if (c=='\n' ||c=='\r') {
			    c = getc(arquivo);
			    while(c=='#') {
				    fscanf(arquivo, "%[^\n\r] ", comentarios);
				    c = getc(arquivo);
			    }
			    ungetc(c,arquivo); 
		    }
		    return 1;
        } else 
            return 0;
    } else 
        return 0;

}

/* Carrega a imagem do arquivo para a estrutura alocada */
imagem_gs *carregarImagemPGM(FILE *arquivo) {

    int lin, col, max;

    if (!testaFormatoImagem(arquivo)) {
        printf("\n - Erro: imagem não está no formato .PPM P5 \n\n"); 
        return NULL;
    }
    
    /* Obtem o width e o height da imagem, e o valor maximo de cada componente RGB */
    fscanf(arquivo, "%d %d %d", &col, &lin, &max);
    
    imagem_gs *img = alocaImagem(lin, col, max);
    
    /* Le os pixels da imagem */
    size_t tamanho = fread(img->pixels, sizeof(pixel), lin * col, arquivo);
    if ( tamanho < lin * col ) {
        liberaImagem(img);
        return NULL;
    }
    
    return img;
}

/* Salva a imagem_gs em um arquivo .ppm */
void salvarImagem(imagem_gs *img, char *nomeSaida) {

    int i;
    
    FILE *saida = fopen(nomeSaida,"wb");
    if (!saida) {
        printf("\n - Erro ao gerar o arquivo de saida. \n\n");
        return;
    }
    
    fprintf(saida, "P5\n%d %d\n%d",img->col, img->lin, img->max);
    fwrite(img->pixels, img->lin * img->col, img->lin * img->col, saida);
    
    fclose(saida);
}

/* Aplica o filtro smooth na imagem original e salva em uma nova imagem */
int filtroSmooth(imagem_gs *original, imagem_gs *nova) {

    int i, j, k, l;
    int numeroPixels;
    int soma;
    
    if (!original || !nova)
        return 0;
    
    for (i=0 ; i < (original->lin) ; i++) {
        for (j=1 ; j< (original->col) ; j++) {

            /* Para cada pixel, analisa os pixels ao seu redor (5x5) */
            numeroPixels = 25;            
            soma = 0;
            
            for (k=(i-2) ; k<=(i+2) ; k++) {
                for (l=(j-2) ; l<=(j+2) ; l++) {
    
                    /* Pixels de borda */
                    if ((k<0) || (k>=original->lin) || (l<0) || (l>=original->col)) {
                        soma += obterPixel(original,i,j);
                    } else {   
                        soma += obterPixel(original,k,l);
                    }
                }
            }

            salvarPixel(nova, i, j, soma / numeroPixels);
                   
        }
    }
    
    /* Copia os pixels de borda da imagem original */
    
    for (i=0 ; i <= 1 ; i++) 
        for (j=0 ; j < original->col ; j++) {
            salvarPixel(nova,i,j,obterPixel(original,i,j));
            salvarPixel(nova,(original->lin - i) -1 ,j,obterPixel(original,(original->lin - i) -1,j));
        }
    
    for (j=0 ; j <= 1 ; j++)
        for (i=0 ; i < original->lin ; i++) {
            salvarPixel(nova,i,j,obterPixel(original,i,j));
            salvarPixel(nova, i ,(original->col - j) -1 ,obterPixel(original,i,(original->col - j) -1));
        } 
    
    return 1;

}


int main(int argc, char **argv) {
    
    imagem_gs *imgOriginal;
    imagem_gs *imgNova;
    
    FILE *arquivo;
    char *nomeDoArquivo;
    char *arquivoSaida;
    
    int lin, col, max, i, j, P;

	/* Variáveis para calcular o tempo entre tarefas */
	struct timeval start, end;
    double time_spent; 

    /* Nome do Arquivo */
    if (argc > 1) {
        nomeDoArquivo = (char *)malloc(sizeof(argv[1]));
        strcpy(nomeDoArquivo, argv[1]);
    } else {
        nomeDoArquivo = (char *)malloc(7 * sizeof(char));
        strcpy(nomeDoArquivo, "in.pgm");
    }

    /* Tenta abrir a imagem */

    gettimeofday(&start, NULL);

    arquivo = fopen(nomeDoArquivo, "rb");
    if (!arquivo) {
        printf("\n - Erro ao abrir a imagem %s. Programa abortado.\n\n",nomeDoArquivo);
        return 1;
    }
    
    imgOriginal = carregarImagemPGM(arquivo);
    if (!imgOriginal) {
        printf("\n - Erro ao carregar a imagem. \n\n");
        return 1;
    }
    
    imgNova = alocaImagem(imgOriginal->lin, imgOriginal->col, imgOriginal->max);
    if (!imgNova) {
        printf("\n - Erro ao aplicar o filtro \n\n");
        return 1;
    }

	gettimeofday(&end, NULL);
    time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;    
    printf("\n - Tempo de leitura e armazenamento da imagem: %lfs",time_spent);
    

    gettimeofday(&start, NULL);
    
    if (filtroSmooth(imgOriginal, imgNova)) {

		gettimeofday(&end, NULL);
        time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;
        printf("\n - Tempo para aplicar o filtro: %lfs",time_spent);     
		
        if (argc > 2) {
            arquivoSaida = (char *)malloc(sizeof(argv[2]));
            strcpy(arquivoSaida, argv[2]);
        } else {
            arquivoSaida = (char *)malloc(8 * sizeof(char));
            strcpy(arquivoSaida, "out.pgm");
        }

		gettimeofday(&start, NULL);           

        salvarImagem(imgNova, arquivoSaida);

		gettimeofday(&end, NULL);
        time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;
        printf("\n - Tempo para salvar a imagem: %lfs \n\n",time_spent);

    } else {
        printf("\n - Erro ao aplicar o filtro \n\n");
        return 1;    
    }
    
    liberaImagem(imgNova);
    liberaImagem(imgOriginal);  
    
    return 0;
    
}
