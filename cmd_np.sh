mpirun -hostfile hosts10 -np 2 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np2-threads3.txt
mpirun -hostfile hosts10 -np 4 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np4-threads3.txt
mpirun -hostfile hosts10 -np 6 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np6-threads3.txt
mpirun -hostfile hosts10 -np 8 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np8-threads3.txt
mpirun -hostfile hosts10 -np 10 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np10-threads3.txt
mpirun -hostfile hosts10 -np 20 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np20-threads3.txt
mpirun -hostfile hosts10 -np 40 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np40-threads3.txt
mpirun -hostfile hosts10 -np 60 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np60-threads3.txt
mpirun -hostfile hosts10 -np 80 ./paralelo/paralelo ./imagens/1.ppm ./resultados/np/foto1.ppm 3 > ./resultados/np/foto1-hosts10-np80-threads3.txt
mpirun -hostfile hosts10 -np 2 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np2-threads3.txt
mpirun -hostfile hosts10 -np 4 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np4-threads3.txt
mpirun -hostfile hosts10 -np 6 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np6-threads3.txt
mpirun -hostfile hosts10 -np 8 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np8-threads3.txt
mpirun -hostfile hosts10 -np 10 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np10-threads3.txt
mpirun -hostfile hosts10 -np 20 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np20-threads3.txt
mpirun -hostfile hosts10 -np 40 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np40-threads3.txt
mpirun -hostfile hosts10 -np 60 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np60-threads3.txt
mpirun -hostfile hosts10 -np 80 ./paralelo/paralelo ./imagens/2.ppm ./resultados/np/foto2.ppm 3 > ./resultados/np/foto2-hosts10-np80-threads3.txt
mpirun -hostfile hosts10 -np 2 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np2-threads3.txt
mpirun -hostfile hosts10 -np 4 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np4-threads3.txt
mpirun -hostfile hosts10 -np 6 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np6-threads3.txt
mpirun -hostfile hosts10 -np 8 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np8-threads3.txt
mpirun -hostfile hosts10 -np 10 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np10-threads3.txt
mpirun -hostfile hosts10 -np 20 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np20-threads3.txt
mpirun -hostfile hosts10 -np 40 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np40-threads3.txt
mpirun -hostfile hosts10 -np 60 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np60-threads3.txt
mpirun -hostfile hosts10 -np 80 ./paralelo/paralelo ./imagens/3.ppm ./resultados/np/foto3.ppm 3 > ./resultados/np/foto3-hosts10-np80-threads3.txt
mpirun -hostfile hosts10 -np 2 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np2-threads3.txt
mpirun -hostfile hosts10 -np 4 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np4-threads3.txt
mpirun -hostfile hosts10 -np 6 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np6-threads3.txt
mpirun -hostfile hosts10 -np 8 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np8-threads3.txt
mpirun -hostfile hosts10 -np 10 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np10-threads3.txt
mpirun -hostfile hosts10 -np 20 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np20-threads3.txt
mpirun -hostfile hosts10 -np 40 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np40-threads3.txt
mpirun -hostfile hosts10 -np 60 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np60-threads3.txt
mpirun -hostfile hosts10 -np 80 ./paralelo/paralelo ./imagens/4.ppm ./resultados/np/foto4.ppm 3 > ./resultados/np/foto4-hosts10-np80-threads3.txt
