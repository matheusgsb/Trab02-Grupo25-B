#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h> 
#include <time.h>
#include "mpi.h"

typedef unsigned char pixel;

typedef struct {
    int lin;
    int col;
    int max;
    pixel *pixels;
} imagem;

typedef struct {
    int inicio, fim;
} intervalo;

/* Aloca espaco na memoria para uma imagem com 'lin' linhas e 'col' colunas */
imagem *alocaImagem(int lin, int col, int max) {

    imagem *img = (imagem *)malloc(sizeof(imagem));
    if (!img)
        return NULL;
        
    img->lin = lin;
    img->col = col;
    img->max = max;
    img->pixels = (pixel *)malloc(lin * col * sizeof(pixel));
    if (!img->pixels)
        return NULL;
        
    return img;

}

/* Libera o espaco alocado para a imagem */
void liberaImagem(imagem *img) {
    if (img != NULL) {
        free(img->pixels);
        free(img);
    }
}

/* Simula o acesso de uma matriz, retornando o pixel na posicao (i,j) */
pixel obterPixel(imagem *img, int i, int j) {
    
    return img->pixels[(img->col * i) + j];    
  
}

/* Altera o pixel (i,j) na imagem */
void salvarPixel(imagem *img, int i, int j, pixel P) {

    img->pixels[(img->col * i) + j] = P;
}


/* Verifica se a imagem e PPM e esta no formato P6 */
int testaFormatoImagem(FILE *arquivo) {

    char c;
    char comentarios[1024];
    
    c = getc(arquivo);
    if (c=='P' || c=='p') {
        c = getc(arquivo);
        if (c=='5') {
            c = getc(arquivo);
            if (c=='\n' ||c=='\r') {
			    c = getc(arquivo);
			    while(c=='#') {
				    fscanf(arquivo, "%[^\n\r] ", comentarios);
				    c = getc(arquivo);
			    }
			    ungetc(c,arquivo); 
		    }
		    return 1;
        } else 
            return 0;
    } else 
        return 0;

}

/* Carrega a imagem do arquivo para a estrutura alocada */
imagem *carregarImagemPPM(FILE *arquivo) {

    int lin, col, max;

    if (!testaFormatoImagem(arquivo)) {
        printf("\n - Erro: Imagem não está no formato .PPM P5 \n\n"); 
        return NULL;
    }
    
    /* Obtem o width e o height da imagem, e o valor maximo de cada componente RGB */
    fscanf(arquivo, "%d %d %d", &col, &lin, &max);
    
    imagem *img = alocaImagem(lin, col, max);
    
    /* Le os pixels da imagem */
    size_t tamanho = fread(img->pixels, sizeof(pixel), lin * col, arquivo);
    if ( tamanho < lin * col ) {
        liberaImagem(img);
        return NULL;
    }
    
    return img;
}

/* Salva a imagem em um arquivo .ppm */
void salvarImagem(imagem *img, char *nomeSaida) {

    int i;
    
    FILE *saida = fopen(nomeSaida,"wb");
    if (!saida) {
        printf("\n - Erro ao gerar o arquivo de saida. \n\n");
        return;
    }
    
    fprintf(saida, "P5\n%d %d\n%d",img->col, img->lin, img->max);
    fwrite(img->pixels, img->lin * img->col, img->lin * img->col, saida);
    
    fclose(saida);
}

/* Aplica o filtro smooth na imagem original e salva em uma nova imagem */
int filtroSmooth(imagem *original, imagem *nova, intervalo interv) {

    int i, j, k, l;
    int soma;
       
    if (!original || !nova)
        return 0;    
    
    for (i=interv.inicio ; i <= interv.fim ; i++) {
        for (j= 0 ; j< (original->col) ; j++) {

            soma = 0;           
            
                for (k=(i-2) ; k<=(i+2) ; k++) {
                    for (l=(j-2) ; l<=(j+2) ; l++) {

                        if ((k<0) || (k>=original->lin) || (l<0) || (l>=original->col))
                            soma += obterPixel(original,i,j);
                        else 
                            soma += obterPixel(original,k,l);
                    }
                }
                        
                salvarPixel(nova, i, j, soma/25);
            
        }
    }
    
    /* Copia os pixels de borda da imagem original */
    
    for (i=0 ; i <= 1 ; i++) 
        for (j=0 ; j < original->col ; j++) {
            salvarPixel(nova,i,j, obterPixel(original,i,j) );
            salvarPixel(nova,(original->lin - i) -1 ,j, obterPixel(original,(original->lin - i) -1,j));
        }
    
    for (j=0 ; j <= 1 ; j++)
        for (i=0 ; i < original->lin ; i++) {
            salvarPixel(nova,i,j, obterPixel(original,i,j));
            salvarPixel(nova, i ,(original->col - j) -1 ,obterPixel(original,i,(original->col - j) -1));
        } 
    
    
    
    return 1;

}


int main(int argc, char **argv) {
    
    imagem *imgOriginal = NULL;
    imagem *imgNova = NULL;
    
    intervalo *intervalos = NULL;
    unsigned char *resultadoGather = NULL;
    unsigned char *resultado = NULL;
    intervalo interv;
    int *contador, *offset;
    
    FILE *arquivo;
    char *nomeDoArquivo;
    char *arquivoSaida;
    
    int lin, col, max, i, j, n, filtro = 0, tag = 1;
    int rank, rc; 
    
    struct timeval start, end;
    double time_spent;    

    MPI_Status status;

    rc = MPI_Init(&argc,&argv);
    
    if (rc != MPI_SUCCESS) {
        printf("\n - Erro ao paralelizar o programa. \n\n");
        return 1;
    }
    
    MPI_Comm_size(MPI_COMM_WORLD, &n);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    /* Nome do Arquivo */
    if (argc > 1) {
        nomeDoArquivo = (char *)malloc(sizeof(argv[1]));
        strcpy(nomeDoArquivo, argv[1]);
    } else {
        nomeDoArquivo = (char *)malloc(7 * sizeof(char));
        strcpy(nomeDoArquivo, "in.pgm");
    }

    /* Tenta abrir a imagem */

    gettimeofday(&start, NULL);

    arquivo = fopen(nomeDoArquivo, "rb");
    if (!arquivo) {
        printf("\n - Erro ao abrir a imagem %s. Programa abortado.\n\n",nomeDoArquivo);
        return 1;
    }
    
    imgOriginal = carregarImagemPPM(arquivo);
    if (!imgOriginal) {
        printf("\n - Erro ao carregar a imagem. \n\n");
        return 1;
    }
    
    imgNova = alocaImagem(imgOriginal->lin, imgOriginal->col, imgOriginal->max);
    if (!imgNova) {
        printf("\n - Erro ao carregar a imagem \n\n");
        return 1;
    }
    
    gettimeofday(&end, NULL);
    time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;    
    if (rank == 0)
        printf("\n - Tempo de leitura e armazenamento da imagem: %lfs",time_spent);

    contador = (int *)malloc(n*sizeof(int));
    if (!contador) {
        printf("\n - Erro ao aplicar o filtro. \n\n");
        return 1;
    }
        
    offset = (int *)malloc(n*sizeof(int));
    if (!offset) {
        printf("\n - Erro ao aplicar o filtro. \n\n");
        return 1;
    }   
   
    /* Master */
    if (rank == 0) {

        gettimeofday(&start, NULL);


        /* Aloca espaço para os intervalos */
        intervalos = (intervalo *)malloc(n*sizeof(intervalo)); 
        if (!intervalos) {
            printf("\n - Erro ao aplicar o filtro. \n\n");
            return 1;
        }
                
        offset[0] = 0;
        
        /* Calcula os intervalos (linhas) a partir do numero de processos */
        for (i=0;i<n;i++) {
            
            intervalos[i].inicio = ((imgOriginal->lin)/n)*i;
            if(i != n-1)
                intervalos[i].fim = intervalos[i].inicio + ((imgOriginal->lin)/n) - 1;
            else
                intervalos[i].fim = imgOriginal->lin - 1;
    
            contador[i]=(intervalos[i].fim - intervalos[i].inicio +1) * imgNova->col;
            
            if (i>0)
                offset[i] = offset[i-1] + contador[i-1];

        }
     
        /* Aloca espaço para o vetor de resultado do filtro */
        resultado = (unsigned char *)malloc((intervalos[rank].fim - intervalos[rank].inicio +1) * imgNova->col * sizeof(unsigned char));
        if (!resultado) {
            printf("\n - Erro ao aplicar o filtro. \n\n");
            return 1;    
        }
        
        
        /* Envia os intervalos para os outros processos */
        for (i=1; i<n; i++) {
            rc = MPI_Send(&(intervalos[i]), 1 , MPI_2INT, i, tag, MPI_COMM_WORLD);
        }
        
        
        /* Aplica o filtro smooth a partir de um intervalo  */
        filtro = filtroSmooth(imgOriginal, imgNova, intervalos[rank]);     
        
         /* Só junta a informação se houver mais de um processo */
         if (n>1) {

            /* Aloca espaço para o vetor que receberá todos os resultados dos processos */        
            resultadoGather = (unsigned char *)malloc(( imgNova->lin * imgNova->col) * sizeof(unsigned char));
            if (!resultadoGather) {
                printf("\n - Erro ao aplicar o filtro. \n\n");
                return 1;
            }
            
            /* Preenche o vetor com os resultados do filtro, para depois juntar tudo */
            int cont = 0;
            int inicio, fim;
            inicio = intervalos[rank].inicio * imgNova->col; 
            fim = (intervalos[rank].fim * imgNova->col) + imgNova->col;
            
            for (i= inicio ; i< fim; i++) {
                resultado[cont]   = imgNova->pixels[i]; 
				cont += 1;
            }
            
            /* Usa a mesma variavel 'interv' dos processos filhos para garantir que o calculo do tamanho
                do vetor no Gather funcione corretamente. */
            interv.inicio = intervalos[rank].inicio;
            interv.fim = intervalos[rank].fim;
               
          }
        
        /* Demais processos */
        } else if (rank > 0 && rank < n) {
    
            int source = 0;
    
            /* Processo recebe o intervalo do master */
            rc = MPI_Recv(&interv, 1, MPI_2INT, source, tag, MPI_COMM_WORLD, &status);
		    
            /* Aplica o filtro smooth a partir de um intervalo */
            filtro = filtroSmooth(imgOriginal, imgNova, interv);
            
            /* Aloca espaço para armazenar os resultados do filtro */
            resultado = (unsigned char *)malloc((interv.fim - interv.inicio +1) *imgNova->col * sizeof (unsigned char));
            if (!resultado) {
                printf("\n - Erro ao aplicar o filtro. \n\n");
                return 1;    
            }
            
            
            /* Preenche o vetor com os resultados do filtro (nós filho) para depois juntar tudo */
            int cont = 0;
            int inicio, fim;
            inicio = interv.inicio * imgNova->col; 
            fim = (interv.fim * imgNova->col) + imgNova->col;
            
            for (i= inicio ; i< fim; i++) {
                resultado[cont]   = imgNova->pixels[i];
                cont+=1;   
            }
            

        }

        /* Executa tanto para nó filho como master, desde que haja mais de um processo */
        if (n>1) {
            
            int raiz = 0;
            int size = (interv.fim - interv.inicio +1) * imgNova->col;            
        
            /* Junta todas as partes da imagem no vetor ResultadoGather */

            rc = MPI_Gatherv(resultado,size,MPI_UNSIGNED_CHAR,resultadoGather, contador, offset , MPI_UNSIGNED_CHAR,raiz,MPI_COMM_WORLD);
            
            if (rc != MPI_SUCCESS) {
                printf("\n - Gather #%d falou. \n\n",rank);
                return 1;
            }

            /* Só junta no master */
            if (rank == 0) {
            
                int cont = 0;
                for (i = 0; i < imgNova->lin * imgNova->col; i++) {
                    imgNova->pixels[cont] = resultadoGather[i];
                    cont++;
                }
                
            }
                
        }
    
    if (rank == 0 && filtro) {
        
        gettimeofday(&end, NULL);
        time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;    
        
        printf("\n - Tempo para aplicar o filtro: %lfs",time_spent);
    
	    if (argc > 2) {
            arquivoSaida = (char *)malloc(sizeof(argv[2]));
            strcpy(arquivoSaida, argv[2]);
        } else {
            arquivoSaida = (char *)malloc(10 * sizeof(char));
            strcpy(arquivoSaida, "out.pgm" );
        }
        
        gettimeofday(&start, NULL);    
        
        salvarImagem(imgNova, arquivoSaida);
        
        gettimeofday(&end, NULL);
        time_spent = ((end.tv_sec  - start.tv_sec) * 1000000u +  end.tv_usec - start.tv_usec) / 1.e6;
        printf("\n - Tempo para salvar a imagem: %lfs \n\n",time_spent);
            
        liberaImagem(imgNova);
        liberaImagem(imgOriginal);
        
        if (resultado != NULL)  
            free(resultado);
        if (resultadoGather != NULL)
         free(resultadoGather);

		MPI_Finalize();
        return 0;    
            
    }
    
    liberaImagem(imgNova);
    liberaImagem(imgOriginal);  
    if (resultado != NULL)  
        free(resultado);
    
    MPI_Finalize();
    return 0;
    
}
